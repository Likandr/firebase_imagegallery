package com.likandr.firebase_imagegallery.data.user.repositories;

import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.likandr.firebase_imagegallery.common.base.datasource.BaseDataSource;
import com.likandr.firebase_imagegallery.data.user.entities.UserEntity;
import com.likandr.firebase_imagegallery.firebase.RxFirebase;

import java.util.concurrent.Callable;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;

public class UserDataSourceRemote extends BaseDataSource {

    private FirebaseDatabase firebaseDatabase;
    private FirebaseAuth firebaseAuth;

    @Inject
    public UserDataSourceRemote(FirebaseAuth firebaseAuth, FirebaseDatabase firebaseDatabase) {
        this.firebaseDatabase = firebaseDatabase;
        this.firebaseAuth = firebaseAuth;
    }

    public Observable<AuthResult> register(final UserEntity userEntity) {
        return RxFirebase.getObservable(firebaseAuth.createUserWithEmailAndPassword(userEntity.getEmail(),
                userEntity.getPassword()));
    }

    public Observable<AuthResult> login(final UserEntity userEntity) {
        return RxFirebase.getObservable(firebaseAuth.signInWithEmailAndPassword(userEntity.getEmail(),
                userEntity.getPassword()));
    }

    public Observable<Boolean> isUserLogged() {
        return Observable.defer(new Callable<ObservableSource<? extends Boolean>>() {
            @Override
            public ObservableSource<? extends Boolean> call() throws Exception {
                return Observable.just(firebaseAuth.getCurrentUser()!=null);
            }
        });
    }

    public Observable<Void> logoutUser() {
        return Observable.defer(new Callable<ObservableSource<? extends Void>>() {
            @Override
            public ObservableSource<? extends Void> call() throws Exception {
                firebaseAuth.signOut();
                return Observable.just(null);
            }
        });
    }

    public Observable<Object> writeUserInDatabase(final UserEntity userEntity) {

        DatabaseReference targetChild = this.firebaseDatabase.
                getReference()
                .child(FIREBASE_CHILD_KEY_USERS)
                .child(userEntity.getUid());

        return RxFirebase.getObservable(targetChild.setValue(userEntity), new RxFirebase.FirebaseTaskResponseSuccess());
    }
}