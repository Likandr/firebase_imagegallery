package com.likandr.firebase_imagegallery.data.image.entities;

import android.support.annotation.Keep;

import com.google.firebase.database.ServerValue;
import com.google.gson.annotations.SerializedName;

import java.util.HashMap;

@Keep
public class ImageEntity {

    @SerializedName("id") private String id;
    @SerializedName("title") private String title;
    @SerializedName("descr") private String descr;
    @SerializedName("url_small") private String url_small;
    @SerializedName("url_original") private String url_original;
    @SerializedName("timestamp") private long timestamp;

    public ImageEntity() {}

    public ImageEntity(String id, String title, String descr, String url_small, String url_original) {
        this.id = id;
        this.title = title;
        this.descr = descr;
        this.url_small = url_small;
        this.url_original = url_original;
        initTimstamp();
    }

    private void initTimstamp() {
        HashMap<String, Object> dateLastChangedObj = new HashMap<>();
        dateLastChangedObj.put("date", ServerValue.TIMESTAMP);
        this.timestamp = (long) dateLastChangedObj.get("date");
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescr() {
        return descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    public String getUrl_small() {
        return url_small;
    }

    public void setUrl_small(String url_small) {
        this.url_small = url_small;
    }

    public String getUrl_original() {
        return url_original;
    }

    public void setUrl_original(String url_original) {
        this.url_original = url_original;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }
}