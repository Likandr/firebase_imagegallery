package com.likandr.firebase_imagegallery.data.image.repositories;

import com.likandr.firebase_imagegallery.data.image.entities.ImageEntity;
import com.likandr.firebase_imagegallery.data.image.entities.ImageMapper;
import com.likandr.firebase_imagegallery.domain.image.ImageModel;
import com.likandr.firebase_imagegallery.ui.ActivityScope;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;

@ActivityScope
public class ImageDataRepository {
    private final ImageDataSourceRemote imageDataSourceRemote;

    @Inject
    public ImageDataRepository(ImageDataSourceRemote imageDataSourceRemote) {
        this.imageDataSourceRemote = imageDataSourceRemote;
    }

    public Observable createImageData(String title, String descr, String link) {
        ImageEntity imageEntity = new ImageEntity();
        imageEntity.setTitle(title);
        imageEntity.setDescr(descr);
        imageEntity.setUrl_original(link);

        return imageDataSourceRemote.createImageData(imageEntity).map(
                new Function<ImageEntity, ImageModel>() {
            @Override
            public ImageModel apply(@NonNull ImageEntity taskEntity) throws Exception {
                return ImageMapper.transform(taskEntity);
            }
        });
    }

    public Observable deleteImageData(String idKey) {
        ImageEntity imageEntity = new ImageEntity();
        imageEntity.setId(idKey);

        return imageDataSourceRemote.deleteImageData(imageEntity).map(ImageMapper::transform);
    }

    public Observable<ImageModel> getImageData(final String idKey) {
        return imageDataSourceRemote.getImageData(idKey).map(ImageMapper::transform);
    }
}