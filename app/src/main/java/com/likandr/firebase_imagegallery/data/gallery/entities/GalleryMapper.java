package com.likandr.firebase_imagegallery.data.gallery.entities;

import com.likandr.firebase_imagegallery.data.image.entities.ImageEntity;
import com.likandr.firebase_imagegallery.data.image.entities.ImageMapper;
import com.likandr.firebase_imagegallery.domain.gallery.GalleryModel;
import com.likandr.firebase_imagegallery.domain.image.ImageModel;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class GalleryMapper {
    public static GalleryModel transform(GalleryEntity galleryEntity) {
        GalleryModel galleryModel = null;
        if (galleryEntity != null) {
            galleryModel = new GalleryModel();
            if (galleryEntity.getImages() != null) {
                galleryModel.setImages(transform(galleryEntity.getImages()));
            }
        }

        return galleryModel;
    }

    private static HashMap<String, ImageModel> transform(HashMap<String, ImageEntity> images) {
        HashMap<String, ImageModel> result = new HashMap<>();
        Iterator it = images.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, ImageEntity> pair = (Map.Entry)it.next();
            result.put(pair.getKey(), ImageMapper.transform(pair.getValue()));
        }

        return result;
    }
}