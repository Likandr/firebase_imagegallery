package com.likandr.firebase_imagegallery.data.gallery.repositories;

import com.likandr.firebase_imagegallery.data.gallery.entities.GalleryEntity;
import com.likandr.firebase_imagegallery.data.gallery.entities.GalleryMapper;
import com.likandr.firebase_imagegallery.domain.gallery.GalleryModel;
import com.likandr.firebase_imagegallery.ui.ActivityScope;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;
import io.reactivex.functions.Function;

@ActivityScope
public class GalleryRepository {

    private final GalleryDataSourceRemote galleryDataSourceRemote;

    @Inject
    public GalleryRepository(GalleryDataSourceRemote galleryDataSourceRemote) {
        this.galleryDataSourceRemote = galleryDataSourceRemote;
    }

    public Observable<GalleryModel> getGallery() {
        return galleryDataSourceRemote.getGallery().map(new Function<GalleryEntity, GalleryModel>() {
            @Override
            public GalleryModel apply(GalleryEntity galleryEntity) throws Exception {
                return GalleryMapper.transform(galleryEntity);
            }
        });
    }
}