package com.likandr.firebase_imagegallery.data.image.entities;

import com.likandr.firebase_imagegallery.domain.image.ImageModel;

public class ImageMapper {

    public static ImageModel transform(ImageEntity imageEntity) {
        ImageModel imageModel = null;
        if (imageEntity != null) {
            imageModel = new ImageModel();
            imageModel.setId(imageEntity.getId());
            imageModel.setTitle(imageEntity.getTitle());
            imageModel.setDescr(imageEntity.getDescr());
            imageModel.setUrl_small(imageEntity.getUrl_small());
            imageModel.setUrl_original(imageEntity.getUrl_original());
            imageModel.setTimestamp(imageEntity.getTimestamp());
        }

        return imageModel;
    }
}