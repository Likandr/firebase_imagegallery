package com.likandr.firebase_imagegallery.data.user.entities;

import com.google.firebase.auth.AuthResult;
import com.likandr.firebase_imagegallery.domain.user.UserModel;

public class UserMapper {
    public static UserModel transform(UserEntity userEntity) {
        UserModel userModel = null;
        if (userEntity != null) {
            userModel = new UserModel(userEntity.getUsername());
            userModel.setEmail(userEntity.getEmail());
            userModel.setUid(userEntity.getUid());
        }

        return userModel;
    }

    public static UserEntity transform(AuthResult authResult) {
        UserEntity userEntity = null;
        if (authResult != null) {
            userEntity = new UserEntity();
            userEntity.setUid(authResult.getUser().getUid());
            userEntity.setUsername(authResult.getUser().getDisplayName());
            userEntity.setUsername(authResult.getUser().getEmail());
        }

        return userEntity;
    }
}