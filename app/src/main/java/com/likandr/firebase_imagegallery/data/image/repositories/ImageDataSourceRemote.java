package com.likandr.firebase_imagegallery.data.image.repositories;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.likandr.firebase_imagegallery.common.base.datasource.BaseDataSource;
import com.likandr.firebase_imagegallery.data.image.entities.ImageEntity;
import com.likandr.firebase_imagegallery.firebase.RxFirebase;

import java.util.concurrent.Callable;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;

public class ImageDataSourceRemote extends BaseDataSource {

    private DatabaseReference childReference = null;

    private FirebaseDatabase firebaseDatabase;
    private FirebaseAuth firebaseAuth;

    @Inject
    public ImageDataSourceRemote(FirebaseAuth firebaseAuth, FirebaseDatabase firebaseDatabase) {
        this.firebaseDatabase = firebaseDatabase;
        this.firebaseAuth = firebaseAuth;
    }

    public DatabaseReference getChildReference() {
        if (this.childReference == null) {
            this.childReference = this.firebaseDatabase.
                    getReference()
                    .child(FIREBASE_CHILD_KEY_USERS)
                    .child(this.firebaseAuth.getCurrentUser().getUid())
                    .child(FIREBASE_CHILD_KEY_IMAGES);
        }

        return childReference;
    }

    public Observable createImageData(final ImageEntity imageEntity) {
        final String key = this.getChildReference().push().getKey();
        imageEntity.setId(key);

        return RxFirebase.getObservable(getChildReference().child(key).setValue(imageEntity), imageEntity);
    }

    public Observable<ImageEntity> deleteImageData(final ImageEntity imageEntity) {
        this.getChildReference()
                .child(imageEntity.getId())
                .removeValue();

        return Observable.defer(new Callable<ObservableSource<ImageEntity>>() {
            @Override
            public ObservableSource<ImageEntity> call() throws Exception {
                return Observable.just(imageEntity);
            };
        });
    }

    public Observable<ImageEntity> getImageData(final String idKey) {
        return RxFirebase.getObservable(getChildReference().child(idKey), ImageEntity.class);
    }
}
