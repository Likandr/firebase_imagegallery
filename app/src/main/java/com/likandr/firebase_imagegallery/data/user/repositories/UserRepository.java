package com.likandr.firebase_imagegallery.data.user.repositories;

import com.google.firebase.auth.AuthResult;
import com.likandr.firebase_imagegallery.data.user.entities.UserEntity;
import com.likandr.firebase_imagegallery.data.user.entities.UserMapper;
import com.likandr.firebase_imagegallery.domain.user.UserModel;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;
import io.reactivex.functions.Function;

@Singleton
public class UserRepository {

    private final UserDataSourceRemote userDataSourceRemote;

    @Inject
    public UserRepository(UserDataSourceRemote userDataSourceRemote) {
        this.userDataSourceRemote = userDataSourceRemote;
    }

    public Observable<UserModel> register(final String email,
                                          final String password) {
        return this.userDataSourceRemote.register(new UserEntity(email, password))
                .map(new Function<AuthResult, UserEntity>() {
                    @Override
                    public UserEntity apply(AuthResult authResult) throws Exception {
                        return UserMapper.transform(authResult);
                    }
                })
                .map(new Function<UserEntity, UserModel>() {
                    @Override
                    public UserModel apply(UserEntity userEntity) throws Exception {
                        return UserMapper.transform(userEntity);
                    }
                });
    }

    public Observable<UserModel> login(final String email,
                                       final String password) {
        return this.userDataSourceRemote.login(new UserEntity(email, password))
                .map(new Function<AuthResult, UserEntity>() {
                    @Override
                    public UserEntity apply(AuthResult authResult) throws Exception {
                        return UserMapper.transform(authResult);
                    }
                })
                .map(new Function<UserEntity, UserModel>() {
                    @Override
                    public UserModel apply(UserEntity userEntity) throws Exception {
                        return UserMapper.transform(userEntity);
                    }
                });
    }

    public Observable<Boolean> isUserLogged() {
        return this.userDataSourceRemote.isUserLogged();
    }

    public Observable<Void> logoutUser() {
        this.userDataSourceRemote.logoutUser();
        return null;
    }

    public Observable<Object> writeUserInDatabase(final String uid,
                                                  final String username) {
        UserEntity userEntity = new UserEntity();
        userEntity.setUid(uid);
        userEntity.setUsername(username);
        return this.userDataSourceRemote.writeUserInDatabase(userEntity);
    }
}