package com.likandr.firebase_imagegallery.data.gallery.repositories;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.likandr.firebase_imagegallery.common.base.datasource.BaseDataSource;
import com.likandr.firebase_imagegallery.data.gallery.entities.GalleryEntity;
import com.likandr.firebase_imagegallery.firebase.RxFirebase;

import javax.inject.Inject;

import io.reactivex.Observable;

public class GalleryDataSourceRemote extends BaseDataSource {
    private DatabaseReference childReference = null;

    private FirebaseDatabase firebaseDatabase;
    private FirebaseAuth firebaseAuth;

    @Inject
    public GalleryDataSourceRemote(FirebaseAuth firebaseAuth, FirebaseDatabase firebaseDatabase) {
        this.firebaseDatabase = firebaseDatabase;
        this.firebaseAuth = firebaseAuth;
    }

    public DatabaseReference getChildReference() {
        if (childReference == null) {
            this.childReference = this.firebaseDatabase.
                    getReference()
                    .child(FIREBASE_CHILD_KEY_USERS)
                    .child(this.firebaseAuth.getCurrentUser().getUid());
        }

        return childReference;
    }

    public Observable<GalleryEntity> getGallery() {
        return RxFirebase.getObservable(getChildReference(), GalleryEntity.class);
    }
}