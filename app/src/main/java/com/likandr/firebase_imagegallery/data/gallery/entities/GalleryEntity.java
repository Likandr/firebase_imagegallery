package com.likandr.firebase_imagegallery.data.gallery.entities;

import android.support.annotation.Keep;

import com.google.firebase.database.Exclude;
import com.google.gson.annotations.SerializedName;
import com.likandr.firebase_imagegallery.data.image.entities.ImageEntity;

import java.util.HashMap;

@Keep
public final class GalleryEntity {
    @SerializedName("images") private HashMap<String, ImageEntity> images = null;

    public GalleryEntity() {
        // Default constructor required for calls to DataSnapshot.getValue(ImageModel.class)
    }

    public void setImages(HashMap<String, ImageEntity> images) {
        this.images = images;
    }

    public HashMap<String, ImageEntity> getImages() {
        return images;
    }

    @Exclude
    public boolean isEmpty() {
        return (this.images == null || this.images.isEmpty());
    }
}
