package com.likandr.firebase_imagegallery.domain.image.interactors;

import com.likandr.firebase_imagegallery.common.base.interactor.BaseUseCase;
import com.likandr.firebase_imagegallery.common.misc.Params;
import com.likandr.firebase_imagegallery.data.image.repositories.ImageDataRepository;

import javax.inject.Inject;

import io.reactivex.Observable;

public class AddImageUseCase extends BaseUseCase {

    public final static String PARAMS_KEY_TITLE = "title";
    public final static String PARAMS_KEY_DESCR = "desrc";
    public final static String PARAMS_KEY_URL = "url";

    public final static String PARAMS_KEY_URLSMALL = "title";
    public final static String PARAMS_KEY_ID = "title";
    public final static String PARAMS_KEY_TIMESTAMP = "title";

    private final ImageDataRepository imageDataRepository;

    @Inject
    public AddImageUseCase(ImageDataRepository imageDataRepository) {
        this.imageDataRepository = imageDataRepository;
    }

    @Override public Observable getObservable(Params params) {
        return this.imageDataRepository.createImageData(
                params.getString(PARAMS_KEY_TITLE, "Title"),
                params.getString(PARAMS_KEY_DESCR, "Description"),
                params.getString(PARAMS_KEY_URL, "https://www.fillmurray.com/140/100"));
    }
}