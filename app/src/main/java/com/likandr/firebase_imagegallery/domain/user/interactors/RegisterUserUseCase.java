package com.likandr.firebase_imagegallery.domain.user.interactors;

import com.likandr.firebase_imagegallery.data.user.repositories.UserRepository;
import com.likandr.firebase_imagegallery.common.misc.Params;
import com.likandr.firebase_imagegallery.common.base.interactor.BaseUseCase;

import javax.inject.Inject;

import io.reactivex.Observable;

public class RegisterUserUseCase extends BaseUseCase {

    private final UserRepository userRepository;

    private final static String PARAMS_KEY_EMAIL = "param_email";
    private final static String PARAMS_KEY_PASSWORD = "param_password";

    @Inject
    public RegisterUserUseCase(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override public Observable getObservable(Params params) {
        return this.userRepository.register(params.getString(PARAMS_KEY_EMAIL, null),
                params.getString(PARAMS_KEY_PASSWORD, null));
    }
}