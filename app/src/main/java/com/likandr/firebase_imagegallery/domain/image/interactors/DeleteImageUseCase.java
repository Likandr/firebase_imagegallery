package com.likandr.firebase_imagegallery.domain.image.interactors;


import com.likandr.firebase_imagegallery.common.base.interactor.BaseUseCase;
import com.likandr.firebase_imagegallery.common.misc.Params;
import com.likandr.firebase_imagegallery.data.image.repositories.ImageDataRepository;

import javax.inject.Inject;

import io.reactivex.Observable;

public class DeleteImageUseCase extends BaseUseCase {

    public final static String PARAMS_KEY_IMAGE_ID = "image_id";

    private final ImageDataRepository imageDataRepository;

    @Inject
    public DeleteImageUseCase(ImageDataRepository imageDataRepository) {
        this.imageDataRepository = imageDataRepository;
    }

    @Override public Observable getObservable(Params params) {
        return this.imageDataRepository.deleteImageData(params.getString(PARAMS_KEY_IMAGE_ID, "0"));
    }
}