package com.likandr.firebase_imagegallery.domain.gallery;

import com.likandr.firebase_imagegallery.domain.image.ImageModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class GalleryModel {

    private HashMap<String, ImageModel> images = new HashMap<>();

    public GalleryModel() {

    }

    public HashMap<String, ImageModel> getImages() {
        return images;
    }

    public void setImages(HashMap<String, ImageModel> images) {
        this.images = images;
    }

    public boolean isEmpty() {
        return (this.images == null || this.images.isEmpty());
    }

    public ArrayList<ImageModel> toDisplayedList() {

        // We sort the list provided by Firebase (ordered by push generated id)
        ArrayList<ImageModel> orderedList = new ArrayList<>(this.images.values());
        Collections.sort(orderedList);
        return orderedList;
    }
}