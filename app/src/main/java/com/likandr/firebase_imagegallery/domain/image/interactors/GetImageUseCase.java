package com.likandr.firebase_imagegallery.domain.image.interactors;

import com.likandr.firebase_imagegallery.common.base.interactor.BaseUseCase;
import com.likandr.firebase_imagegallery.common.misc.Params;
import com.likandr.firebase_imagegallery.data.image.repositories.ImageDataRepository;

import javax.inject.Inject;

import io.reactivex.Observable;

public class GetImageUseCase extends BaseUseCase {

    public final static String PARAMS_KEY_ID_IMAGE = "id_image";

    private final ImageDataRepository imageDataRepository;

    @Inject
    public GetImageUseCase(ImageDataRepository bucketRepository) {
        this.imageDataRepository = bucketRepository;
    }

    @Override public Observable getObservable(Params params) {
        return this.imageDataRepository.getImageData(params.getString(PARAMS_KEY_ID_IMAGE, "0"));
    }
}