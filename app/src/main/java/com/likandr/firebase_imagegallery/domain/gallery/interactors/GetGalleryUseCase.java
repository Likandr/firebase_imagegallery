package com.likandr.firebase_imagegallery.domain.gallery.interactors;

import com.likandr.firebase_imagegallery.common.base.interactor.BaseUseCase;
import com.likandr.firebase_imagegallery.common.misc.Params;
import com.likandr.firebase_imagegallery.data.gallery.repositories.GalleryRepository;

import javax.inject.Inject;

import io.reactivex.Observable;

public class GetGalleryUseCase extends BaseUseCase {

    private final GalleryRepository galleryRepository;

    @Inject
    public GetGalleryUseCase(GalleryRepository bucketRepository) {
        this.galleryRepository = bucketRepository;
    }

    @Override public Observable getObservable(Params params) {
        return this.galleryRepository.getGallery();
    }
}