package com.likandr.firebase_imagegallery.domain.user.interactors;

import com.likandr.firebase_imagegallery.data.user.repositories.UserRepository;
import com.likandr.firebase_imagegallery.common.misc.Params;
import com.likandr.firebase_imagegallery.common.base.interactor.BaseUseCase;

import javax.inject.Inject;

import io.reactivex.Observable;

public class WriteUserUseCase extends BaseUseCase {

    private final UserRepository userRepository;

    public final static String PARAMS_KEY_USERNAME = "param_username";
    public final static String PARAMS_KEY_UID = "param_uid";

    @Inject
    public WriteUserUseCase(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override public Observable getObservable(Params params) {
        return this.userRepository.writeUserInDatabase(params.getString(PARAMS_KEY_UID, null),
                params.getString(PARAMS_KEY_USERNAME, null));
    }
}