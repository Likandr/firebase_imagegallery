package com.likandr.firebase_imagegallery.domain.user.interactors;

import com.likandr.firebase_imagegallery.data.user.repositories.UserRepository;
import com.likandr.firebase_imagegallery.common.misc.Params;
import com.likandr.firebase_imagegallery.common.base.interactor.BaseUseCase;

import javax.inject.Inject;

import io.reactivex.Observable;

public class LoginUserUseCase extends BaseUseCase {

    private final UserRepository userRepository;

    public final static String PARAMS_KEY_EMAIL = "param_email";
    public final static String PARAMS_KEY_PASSWORD = "param_password";

    @Inject
    public LoginUserUseCase(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override public Observable getObservable(Params params) {
        return this.userRepository.login(params.getString(PARAMS_KEY_EMAIL, null),
                params.getString(PARAMS_KEY_PASSWORD, null));
    }
}