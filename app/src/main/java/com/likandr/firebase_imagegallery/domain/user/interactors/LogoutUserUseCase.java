package com.likandr.firebase_imagegallery.domain.user.interactors;

import com.likandr.firebase_imagegallery.data.user.repositories.UserRepository;
import com.likandr.firebase_imagegallery.common.misc.Params;
import com.likandr.firebase_imagegallery.common.base.interactor.BaseUseCase;

import javax.inject.Inject;

import io.reactivex.Observable;

public class LogoutUserUseCase extends BaseUseCase {

    private final UserRepository userRepository;

    @Inject
    public LogoutUserUseCase(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override public Observable getObservable(Params params) {
        return this.userRepository.logoutUser();
    }
}