package com.likandr.firebase_imagegallery.domain.image;

import android.support.annotation.NonNull;

import java.io.Serializable;

public class ImageModel implements Comparable<ImageModel>, Serializable {

    private String id;
    private String title;
    private String descr;
    private String url_small;
    private String url_original;
    private long timestamp;

    public ImageModel() {}

    public ImageModel(String id, String title, String descr, String url_small, String url_original, long timestamp) {
        this.id = id;
        this.title = title;
        this.descr = descr;
        this.url_small = url_small;
        this.url_original = url_original;
        this.timestamp = timestamp;
    }

    @Override
    public int compareTo(@NonNull ImageModel o) {
        return this.timestamp < o.getTimestamp() ? -1 : (this.timestamp == o.getTimestamp() ? 0 : 1);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescr() {
        return descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    public String getUrl_small() {
        return url_small;
    }

    public void setUrl_small(String url_small) {
        this.url_small = url_small;
    }

    public String getUrl_original() {
        return url_original;
    }

    public void setUrl_original(String url_original) {
        this.url_original = url_original;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }
}