package com.likandr.firebase_imagegallery.firebase;

import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.likandr.firebase_imagegallery.common.misc.external.ConfigInterface;

import java.util.HashMap;
import java.util.Map;

public class FirebaseRemoteConfigHelper implements ConfigInterface {

    private FirebaseRemoteConfig instance;

    public FirebaseRemoteConfigHelper(FirebaseRemoteConfig instance) {
        this.instance = instance;
    }

    @Override
    public void setGlobalDefaultValues() {
        Map<String, Object> defaultValues = new HashMap<String, Object>();
        defaultValues.put(
                FirebucketConfig.MAINTENANCE.getKey(),
                FirebucketConfig.MAINTENANCE.getDefaultValue());
        instance.setDefaults(defaultValues);
    }

    @Override
    public void fetch(@NonNull long cacheTTL) {
        final Task<Void> fetch = instance.fetch(cacheTTL);
        fetch.addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                instance.activateFetched();
            }
        });
    }

    @Override
    public String getString(@NonNull String key) {
        return instance.getString(key);
    }

    @Override
    public Double getDouble(@NonNull String key) {
        return instance.getDouble(key);
    }

    @Override
    public boolean getBoolean(@NonNull String key) {
        return instance.getBoolean(key);
    }
}