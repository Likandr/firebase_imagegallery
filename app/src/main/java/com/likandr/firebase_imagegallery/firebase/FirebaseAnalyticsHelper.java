package com.likandr.firebase_imagegallery.firebase;

import android.os.Bundle;
import android.support.annotation.NonNull;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.likandr.firebase_imagegallery.common.misc.external.AnalyticsInterface;

public class FirebaseAnalyticsHelper implements AnalyticsInterface {

    private FirebaseAnalytics instance;

    public FirebaseAnalyticsHelper(FirebaseAnalytics instance) {
        this.instance = instance;
    }

    @Override
    public void trackLoginSuccess(@NonNull Bundle bundle) {
        instance.logEvent("login", bundle);
    }

    @Override
    public void trackLoginFailure(@NonNull Bundle bundle) {
        instance.logEvent("login_fail", bundle);
    }

    @Override
    public void trackRegisterSuccess(@NonNull Bundle bundle) {
        instance.logEvent("register", bundle);
    }

    @Override
    public void trackRegisterFailure(@NonNull Bundle bundle) {
        instance.logEvent("register_fail", bundle);
    }

    @Override
    public void trackGetGallerySuccess(@NonNull Bundle bundle) {
        instance.logEvent("get_gallery", bundle);
    }

    @Override
    public void trackGetGalleryFailure(@NonNull Bundle bundle) {
        instance.logEvent("get_gallery_fail", bundle);
    }

    @Override
    public void trackGetImageDataSuccess(@NonNull Bundle bundle) {
        instance.logEvent("get_image", bundle);
    }

    @Override
    public void trackGetImageDataFailure(@NonNull Bundle bundle) {
        instance.logEvent("get_image_fail", bundle);
    }

    @Override
    public void trackAddImageSuccess(@NonNull Bundle bundle) {
        instance.logEvent("add_image", bundle);
    }

    @Override
    public void trackAddImageFailure(@NonNull Bundle bundle) {
        instance.logEvent("add_image_fail", bundle);
    }

    @Override
    public void trackDeleteImageSuccess(@NonNull Bundle bundle) {
        instance.logEvent("delete_image", bundle);
    }

    @Override
    public void trackDeleteImageFailure(@NonNull Bundle bundle) {
        instance.logEvent("delete_image_fail", bundle);
    }

    @Override
    public void trackPageView(@NonNull String view) {
        instance.logEvent(view, null);
    }
}