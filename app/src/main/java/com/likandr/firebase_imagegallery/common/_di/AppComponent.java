package com.likandr.firebase_imagegallery.common._di;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;
import com.likandr.firebase_imagegallery.common.App;
import com.likandr.firebase_imagegallery.common._di.picasso.PicassoModule;
import com.likandr.firebase_imagegallery.common.misc.external.AnalyticsInterface;
import com.likandr.firebase_imagegallery.common.misc.external.ConfigInterface;
import com.squareup.picasso.Picasso;

import dagger.Component;

@ApplicationScope
@Component(
        modules = {AppModule.class, NetModule.class}
)
public interface AppComponent {

    void inject(App app);

    Context context();
    App app();
    Picasso picasso();
    Gson gson();
    FirebaseDatabase firebaseDatabase();
    FirebaseAuth firebaseAuth();
    AnalyticsInterface analyticsHelper();
    ConfigInterface configHelper();
    SharedPreferences sharedPreferences();
}