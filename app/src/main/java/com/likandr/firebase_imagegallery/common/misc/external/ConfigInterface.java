package com.likandr.firebase_imagegallery.common.misc.external;

import android.support.annotation.NonNull;

public interface ConfigInterface {

    void setGlobalDefaultValues();

    void fetch(@NonNull long cacheTTL);

    String getString(@NonNull String key);
    Double getDouble(@NonNull String key);
    boolean getBoolean(@NonNull String key);
}