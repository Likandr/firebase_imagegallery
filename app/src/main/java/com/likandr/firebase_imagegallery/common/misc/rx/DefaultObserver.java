package com.likandr.firebase_imagegallery.common.misc.rx;

import io.reactivex.observers.DisposableObserver;

public class DefaultObserver<T> extends DisposableObserver<T> {
    @Override public void onNext(T t) {
        // Intentionally empty.
    }

    @Override public void onComplete() {
        // Intentionally empty.
    }

    @Override public void onError(Throwable exception) {
        // Intentionally empty.
    }
}