package com.likandr.firebase_imagegallery.common.base.datasource;

public class BaseDataSource {

    protected final static String FIREBASE_CHILD_KEY_USERS = "users";
    protected final static String FIREBASE_CHILD_KEY_IMAGES = "images";
}