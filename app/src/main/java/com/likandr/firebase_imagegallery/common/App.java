package com.likandr.firebase_imagegallery.common;

import android.app.Application;
import android.content.Context;
import android.os.StrictMode;

import com.likandr.firebase_imagegallery.BuildConfig;
import com.likandr.firebase_imagegallery.common._di.AppComponent;
import com.likandr.firebase_imagegallery.common._di.AppModule;
import com.likandr.firebase_imagegallery.common._di.DaggerAppComponent;
import com.squareup.leakcanary.LeakCanary;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;

public class App extends Application {

    public static final String TAG = App.class.getSimpleName();

    private AppComponent component;

    public AppComponent getComponent() {
        return component;
    }

    private void setAppComponent() {
        component = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }

    public void setComponent(AppComponent applicationComponent) {
        component = applicationComponent;
    }


    @Override
    public void onCreate() {
        super.onCreate();

        if (BuildConfig.DEBUG) {
            enableStrictMode();
            LeakCanary.install(this);
        }

        this.setAppComponent();
        initRemoteConfig();
    }

    public static App get(Context context) {
        return (App) context.getApplicationContext();
    }


    private void enableStrictMode() {
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
                .detectAll()
                .penaltyLog()
                .build());
        StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
                .detectAll()
                .penaltyLog()
                .build());
    }

    private void initRemoteConfig() {
        component.configHelper().setGlobalDefaultValues();
        component.configHelper().fetch(0);
    }
}