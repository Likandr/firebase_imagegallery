package com.likandr.firebase_imagegallery.common._di;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.likandr.firebase_imagegallery.common.App;
import com.likandr.firebase_imagegallery.common.misc.external.AnalyticsInterface;
import com.likandr.firebase_imagegallery.common.misc.external.ConfigInterface;
import com.likandr.firebase_imagegallery.firebase.FirebaseAnalyticsHelper;
import com.likandr.firebase_imagegallery.firebase.FirebaseRemoteConfigHelper;
import com.squareup.picasso.Picasso;

import dagger.Module;
import dagger.Provides;

@Module
public final class AppModule {
    private App app;

    public AppModule(App app) {
        this.app = app;
    }

    @Provides @ApplicationScope
    public App provideApplication() {
        return app;
    }

    @Provides @ApplicationScope
    public Context provideApplicationContext() {
        return app.getApplicationContext();
    }

    @Provides
    @ApplicationScope
    public Picasso getPicasso() {
        return new Picasso.Builder(app).build();
    }

    @Provides @ApplicationScope
    public FirebaseDatabase provideFirebaseDatabase() {
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        firebaseDatabase.setPersistenceEnabled(true);
        return firebaseDatabase;
    }

    @Provides @ApplicationScope
    public FirebaseAuth provideFirebaseAuth() {
        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        return firebaseAuth;
    }

    @Provides @ApplicationScope
    public AnalyticsInterface provideAnalyticsHelper(Context context) {
        AnalyticsInterface AnalyticsInterface = new FirebaseAnalyticsHelper(FirebaseAnalytics.getInstance(context));
        return AnalyticsInterface;
    }

    @Provides
    @ApplicationScope
    public ConfigInterface provideConfigHelper() {
        ConfigInterface configInterface = new FirebaseRemoteConfigHelper(FirebaseRemoteConfig.getInstance());
        return configInterface;
    }

    @Provides
    @ApplicationScope
    public SharedPreferences provideSharedPreferences(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }
}
