package com.likandr.firebase_imagegallery.common.misc;

import android.content.res.Configuration;
import android.support.annotation.NonNull;
import android.util.Log;

import com.likandr.firebase_imagegallery.common.base.BaseView;

public class OrientationUtils {
    private final static String TAG = "OrientationUtils";

    public static void setUpOrientation(@NonNull Configuration config,
                                        @NonNull BaseView view) {
        Log.d(TAG, "onConfigurationChanged");
        if (config.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            view.onLandscape();
        } else if (config.orientation == Configuration.ORIENTATION_PORTRAIT){
            view.onPortrait();
        }
    }
}