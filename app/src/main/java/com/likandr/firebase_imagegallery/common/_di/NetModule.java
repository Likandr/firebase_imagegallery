package com.likandr.firebase_imagegallery.common._di;

import android.app.Application;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.likandr.firebase_imagegallery.BuildConfig;

import java.io.File;
import java.util.concurrent.TimeUnit;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;

@Module
public class NetModule {
    //  OkHttp client request time out.
    private static final int CLIENT_TIME_OUT = 10;

    //  OkHttp cache size.
    private static final int CLIENT_CACHE_SIZE = 10 * 1024 * 1024; // 10 MiB

    //  OkHttp request date format. Eg. 2016-06-19T13:07:45.139Z
    private static final String CLIENT_DATE_FORMAT = "yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'SSS'Z'";

    //  OkHttp cache directory.
    private static final String CLIENT_CACHE_DIRECTORY = "http";

    @Provides @ApplicationScope
    Cache provideCache(Application application) {
        return new Cache(new File(application.getCacheDir(), CLIENT_CACHE_DIRECTORY), CLIENT_CACHE_SIZE);
    }

    @Provides @ApplicationScope
    Gson provideGson() {
        return new GsonBuilder()
                .setDateFormat(CLIENT_DATE_FORMAT)
                .create();
    }

    @Provides
    @ApplicationScope
    OkHttpClient provideOkHttpClient(Cache cache) {
        return new OkHttpClient.Builder()
                .addInterceptor(new HttpLoggingInterceptor()
                        .setLevel(BuildConfig.DEBUG ? HttpLoggingInterceptor.Level.BODY : HttpLoggingInterceptor.Level.NONE))
                .connectTimeout(CLIENT_TIME_OUT, TimeUnit.SECONDS)
                .writeTimeout(CLIENT_TIME_OUT, TimeUnit.SECONDS)
                .readTimeout(CLIENT_TIME_OUT, TimeUnit.SECONDS)
                .cache(cache)
                .build();
    }
}