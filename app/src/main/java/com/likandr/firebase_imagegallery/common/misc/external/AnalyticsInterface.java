package com.likandr.firebase_imagegallery.common.misc.external;

import android.os.Bundle;
import android.support.annotation.NonNull;

public interface AnalyticsInterface {

    public final static String PARAM_USER_UID = "user_uid";
    public final static String PARAM_MESSAGE = "message";

    public final static String VIEW_ONBOARDING = "view_onboarding";
    public final static String VIEW_LOGIN = "view_login";
    public final static String VIEW_REGISTRATION = "view_register";
    public final static String VIEW_SHOWCASE = "view_showcase";
    public final static String VIEW_ADDIMAGE = "view_image_add";
    public final static String VIEW_DETAILS = "view_image_details";

    void trackLoginSuccess(@NonNull Bundle bundle);
    void trackLoginFailure(@NonNull Bundle bundle);

    void trackRegisterSuccess(@NonNull Bundle bundle);
    void trackRegisterFailure(@NonNull Bundle bundle);

    void trackGetGallerySuccess(@NonNull Bundle bundle);
    void trackGetGalleryFailure(@NonNull Bundle bundle);

    void trackGetImageDataSuccess(@NonNull Bundle bundle);
    void trackGetImageDataFailure(@NonNull Bundle bundle);

    void trackAddImageSuccess(@NonNull Bundle bundle);
    void trackAddImageFailure(@NonNull Bundle bundle);

    void trackDeleteImageSuccess(@NonNull Bundle bundle);
    void trackDeleteImageFailure(@NonNull Bundle bundle);

    void trackPageView(@NonNull String view);
}