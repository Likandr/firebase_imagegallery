package com.likandr.firebase_imagegallery.common.base.presenter;

import com.likandr.firebase_imagegallery.common.base.BaseMvpView;

public interface IBasePresenter<V extends BaseMvpView> {

    void attachView(V view);
    void detachView();
    boolean isViewAttached();
    void checkViewAttached();
}