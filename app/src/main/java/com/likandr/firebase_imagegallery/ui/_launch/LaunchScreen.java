package com.likandr.firebase_imagegallery.ui._launch;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.cremy.greenrobotutils.library.ui.ActivityUtils;
import com.likandr.firebase_imagegallery.ui.onboard.OnBoardingActivity;

public class LaunchScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.next();
    }


    private void next() {
        finish();
        ActivityUtils.cancelCloseAnimation(this);
        OnBoardingActivity.startMe(this);
    }
}