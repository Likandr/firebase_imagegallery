package com.likandr.firebase_imagegallery.ui.gallery._di;

import android.support.v4.app.Fragment;

import dagger.Module;
import dagger.Provides;

@Module
public class GalleryModule {

    private Fragment fragment;

    public GalleryModule(Fragment fragment) {
        this.fragment = fragment;
    }

    @Provides
    Fragment provideFragment() {
        return fragment;
    }
}