package com.likandr.firebase_imagegallery.ui.onboard.landing;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.likandr.firebase_imagegallery.common.App;
import com.likandr.firebase_imagegallery.R;
import com.likandr.firebase_imagegallery.common.base.Layout;
import com.likandr.firebase_imagegallery.ui.onboard._di.DaggerUserComponent;
import com.likandr.firebase_imagegallery.ui.onboard._di.UserComponent;
import com.likandr.firebase_imagegallery.ui.onboard._di.UserModule;
import com.likandr.firebase_imagegallery.ui.onboard.OnBoardingMVP;
import com.likandr.firebase_imagegallery.ui.onboard.OnBoardingPresenter;
import com.likandr.firebase_imagegallery.common.base.BaseFragment;
import com.likandr.firebase_imagegallery.ui.onboard.login.LoginFragment;
import com.likandr.firebase_imagegallery.ui.onboard.register.RegisterFragment;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

@Layout(id = R.layout.fragment_onboarding)
public class LandingFragment extends BaseFragment
        implements OnBoardingMVP.View {

    public static final String TAG = LandingFragment.class.getName();

    OnShowMessageListener onShowMessageListener;
    OnChangeFragment onChangeFragment;

    @BindView(R.id.imageview_app_logo)
    ImageView imageViewAppLogo;

    @OnClick(R.id.button_cta_login)
    public void clickCtaLogin() {
        onChangeFragment.onChangeFragment(LoginFragment.TAG, getTransitionPair());
    }
    @OnClick(R.id.textview_cta_register)
    public void clickCtaRegister() {
        onChangeFragment.onChangeFragment(RegisterFragment.TAG, getTransitionPair());
    }

    @Inject
    OnBoardingPresenter presenter;
    UserComponent component;

    @Override
    public void injectDependencies() {
        if (component == null) {
            component = DaggerUserComponent.builder()
                    .appComponent(App.get(getContext()).getComponent())
                    .userModule(new UserModule(this))
                    .build();
            component.inject(this);
        }
    }

    @Override
    public void attachToPresenter() {
        this.presenter.attachView(this);
    }

    @Override
    public void detachFromPresenter() {
        this.presenter.detachView();
    }

    public LandingFragment() {
        // Required empty public constructor
    }

    public static LandingFragment newInstance() {
        LandingFragment fragment = new LandingFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getArgs(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = super.onCreateView(inflater, container, savedInstanceState);

        if (presenter.isInMaintenance()) {
            showMessage("TODO: In maintenance");
        } else {
            presenter.checkIfUserIsLogged();
        }
        return v;
    }

    @Override
    public void onAttach(Context context) {
        this.injectDependencies();
        this.attachToPresenter();
        super.onAttach(context);
        if (context instanceof OnShowMessageListener) {
            onShowMessageListener = (OnShowMessageListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnShowMessageListener");
        }
        if (context instanceof OnChangeFragment) {
            onChangeFragment = (OnChangeFragment) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnChangeFragment");
        }
    }

    @Override
    public void onDetach() {
        detachFromPresenter();
        super.onDetach();
    }

    @Override
    public void getArgs(Bundle _bundle) {
        // Empty intentionally.
    }

    @Override
    public void onLandscape() {
        // Empty intentionally.
    }

    @Override
    public void onPortrait() {
        // Empty intentionally.
    }

    @Override
    public void showLoading() {
        // Empty intentionally.
    }

    @Override
    public void hideLoading() {
        // Empty intentionally.
    }

    @Override
    public void showMessage(String message) {
        if (onShowMessageListener != null) {
            onShowMessageListener.onShowMessage(message);
        }
    }

    @Override
    public void showNoNetwork() {
        // Empty intentionally.
    }

    @Override
    public void userLogged() {
        presenter.goToBucket(getContext());
        getActivity().finish();
    }

    private Pair getTransitionPair() {
        Pair<View, String> pair = Pair.create((View)this.imageViewAppLogo,
                getResources().getString(R.string.transition_logo_onboarding_to_login_and_register));
        return pair;
    }
}