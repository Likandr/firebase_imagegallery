package com.likandr.firebase_imagegallery.ui.onboard._di;

import com.likandr.firebase_imagegallery.data.user.repositories.UserRepository;
import com.likandr.firebase_imagegallery.data.user.repositories.UserDataSourceRemote;
import com.likandr.firebase_imagegallery.common._di.AppComponent;
import com.likandr.firebase_imagegallery.ui.ActivityScope;
import com.likandr.firebase_imagegallery.domain.user.interactors.CheckUserUseCase;
import com.likandr.firebase_imagegallery.domain.user.interactors.LoginUserUseCase;
import com.likandr.firebase_imagegallery.domain.user.interactors.LogoutUserUseCase;
import com.likandr.firebase_imagegallery.domain.user.interactors.RegisterUserUseCase;
import com.likandr.firebase_imagegallery.domain.user.interactors.WriteUserUseCase;
import com.likandr.firebase_imagegallery.ui.onboard.OnBoardingPresenter;
import com.likandr.firebase_imagegallery.ui.onboard.landing.LandingFragment;
import com.likandr.firebase_imagegallery.ui.onboard.login.LoginFragment;
import com.likandr.firebase_imagegallery.ui.onboard.login.LoginPresenter;
import com.likandr.firebase_imagegallery.ui.onboard.register.RegisterFragment;
import com.likandr.firebase_imagegallery.ui.onboard.register.RegisterPresenter;

import dagger.Component;

@ActivityScope
@Component(
        dependencies = AppComponent.class,
        modules = UserModule.class
)
public interface UserComponent {

    //Fragments
    void inject(LandingFragment view);
    void inject(LoginFragment view);
    void inject(RegisterFragment view);

    // Presenters
    void inject(OnBoardingPresenter presenter);
    void inject(LoginPresenter presenter);
    void inject(RegisterPresenter presenter);

    // UseCases/Interactors
    void inject(CheckUserUseCase useCase);
    void inject(LoginUserUseCase useCase);
    void inject(LogoutUserUseCase useCase);
    void inject(RegisterUserUseCase useCase);
    void inject(WriteUserUseCase useCase);

    // Repositories
    void inject(UserRepository repository);

    // DataSources
    void inject(UserDataSourceRemote dataSource);
}