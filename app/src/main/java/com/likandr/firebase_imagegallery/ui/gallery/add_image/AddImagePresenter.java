package com.likandr.firebase_imagegallery.ui.gallery.add_image;

import android.os.Bundle;

import com.likandr.firebase_imagegallery.common.base.presenter.BasePresenter;
import com.likandr.firebase_imagegallery.common.misc.Params;
import com.likandr.firebase_imagegallery.common.misc.external.AnalyticsInterface;
import com.likandr.firebase_imagegallery.common.misc.rx.DefaultObserver;
import com.likandr.firebase_imagegallery.domain.image.ImageModel;
import com.likandr.firebase_imagegallery.domain.image.interactors.AddImageUseCase;

import javax.inject.Inject;

public class AddImagePresenter  extends BasePresenter<AddImageMVP.View>
        implements AddImageMVP.Presenter {

    private final static String TAG = AddImagePresenter.class.getName();

    private final AddImageUseCase addImageUseCase;
    private final AnalyticsInterface analyticsInterface;

    @Inject
    public AddImagePresenter(AddImageUseCase addImageUseCase,
                               AnalyticsInterface analyticsInterface) {
        this.addImageUseCase = addImageUseCase;
        this.analyticsInterface = analyticsInterface;
    }

    @Override
    public void attachView(AddImageMVP.View view) {
        super.attachView(view);
        analyticsInterface.trackPageView(AnalyticsInterface.VIEW_ADDIMAGE);
    }

    @Override
    public void detachView() {
        addImageUseCase.dispose();
        super.detachView();
    }

    @Override
    public void createTask(String title, String descr, String url_original) {
        if (title.isEmpty() || url_original.isEmpty()) {
            checkViewAttached();
            return;
        }

        Params params = Params.create();
        params.putString(AddImageUseCase.PARAMS_KEY_TITLE, title);
        params.putString(AddImageUseCase.PARAMS_KEY_DESCR, descr);
        params.putString(AddImageUseCase.PARAMS_KEY_URL, url_original);

        addImageUseCase.execute(new AddImagePresenter.AddImageObserver(), params);
    }

    @Override
    public void onCreateTaskSuccess(ImageModel imageModel) {
        checkViewAttached();
        view.hideLoading();
        view.onSuccess();
    }

    @Override
    public void onCreateTaskFailure(Throwable e) {
        e.printStackTrace();
        checkViewAttached();
        view.hideLoading();
    }

    @Override
    public void onCreateTaskSuccessTracking(ImageModel imageModel) {
        Bundle bundle = new Bundle();
        bundle.putString(AddImageUseCase.PARAMS_KEY_TITLE, imageModel.getTitle());
        analyticsInterface.trackAddImageSuccess(bundle);
    }

    @Override
    public void onCreateTaskFailureTracking(Throwable e) {
        Bundle bundle = new Bundle();
        bundle.putString(AnalyticsInterface.PARAM_MESSAGE, e.getMessage());
        analyticsInterface.trackAddImageFailure(bundle);
    }

    private final class AddImageObserver extends DefaultObserver<ImageModel> {

        @Override public void onComplete() {
            super.onComplete();
        }

        @Override public void onError(Throwable e) {
            super.onError(e);
            onCreateTaskFailureTracking(e);
            onCreateTaskFailure(e);
        }

        @Override public void onNext(ImageModel taskModel) {
            super.onNext(taskModel);
            onCreateTaskSuccessTracking(taskModel);
            onCreateTaskSuccess(taskModel);
        }
    }
}