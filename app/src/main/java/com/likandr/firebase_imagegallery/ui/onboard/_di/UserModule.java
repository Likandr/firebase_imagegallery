package com.likandr.firebase_imagegallery.ui.onboard._di;

import android.support.v4.app.Fragment;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.likandr.firebase_imagegallery.data.user.repositories.UserRepository;
import com.likandr.firebase_imagegallery.data.user.repositories.UserDataSourceRemote;
import com.likandr.firebase_imagegallery.domain.user.interactors.CheckUserUseCase;
import com.likandr.firebase_imagegallery.domain.user.interactors.LoginUserUseCase;
import com.likandr.firebase_imagegallery.domain.user.interactors.LogoutUserUseCase;
import com.likandr.firebase_imagegallery.domain.user.interactors.RegisterUserUseCase;
import com.likandr.firebase_imagegallery.domain.user.interactors.WriteUserUseCase;
import com.likandr.firebase_imagegallery.common.misc.external.AnalyticsInterface;
import com.likandr.firebase_imagegallery.common.misc.external.ConfigInterface;
import com.likandr.firebase_imagegallery.ui.onboard.login.LoginPresenter;
import com.likandr.firebase_imagegallery.ui.onboard.OnBoardingPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class UserModule {

    private Fragment fragment;

    public UserModule(Fragment fragment) {
        this.fragment = fragment;
    }

    @Provides
    Fragment provideFragment() {
        return fragment;
    }

    @Provides
    OnBoardingPresenter provideOnBoardingPresenter(CheckUserUseCase checkUserUseCase,
                                                   AnalyticsInterface analyticsInterface,
                                                   ConfigInterface configInterface) {
        return new OnBoardingPresenter(checkUserUseCase,
                analyticsInterface,
                configInterface);
    }

    @Provides
    LoginPresenter provideLoginPresenter(LoginUserUseCase loginUserUseCase,
                                         AnalyticsInterface analyticsInterface) {
        return new LoginPresenter(loginUserUseCase, analyticsInterface);
    }

    @Provides
    CheckUserUseCase provideCheckUserUseCase(UserRepository repository) {
        return new CheckUserUseCase(repository);
    }

    @Provides
    LoginUserUseCase provideLoginUserUseCase(UserRepository repository) {
        return new LoginUserUseCase(repository);
    }

    @Provides
    LogoutUserUseCase provideLogoutUserUseCase(UserRepository repository) {
        return new LogoutUserUseCase(repository);
    }

    @Provides
    RegisterUserUseCase provideRegisterUserUseCase(UserRepository repository) {
        return new RegisterUserUseCase(repository);
    }

    @Provides
    WriteUserUseCase provideWriteUserUseCase(UserRepository repository) {
        return new WriteUserUseCase(repository);
    }

    @Provides
    UserDataSourceRemote provideDataSource(FirebaseAuth firebaseAuth,
                                           FirebaseDatabase firebaseDatabase) {
        return new UserDataSourceRemote(firebaseAuth, firebaseDatabase);
    }

    @Provides
    UserRepository provideRepository(UserDataSourceRemote dataSource) {
        return new UserRepository(dataSource);
    }

}