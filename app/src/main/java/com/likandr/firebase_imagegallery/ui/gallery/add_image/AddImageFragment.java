package com.likandr.firebase_imagegallery.ui.gallery.add_image;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Gallery;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.cremy.greenrobotutils.library.ui.SnackBarUtils;
import com.likandr.firebase_imagegallery.R;
import com.likandr.firebase_imagegallery.common.App;
import com.likandr.firebase_imagegallery.common.base.BaseFragment;
import com.likandr.firebase_imagegallery.common.base.Layout;
import com.likandr.firebase_imagegallery.ui.gallery._di.DaggerGalleryComponent;
import com.likandr.firebase_imagegallery.ui.gallery._di.GalleryComponent;
import com.likandr.firebase_imagegallery.ui.gallery.showcase.ShowcaseFragment;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

@Layout(id = R.layout.fragment_imageadd)
public class AddImageFragment extends BaseFragment implements AddImageMVP.View {

    public static final String TAG = AddImageFragment.class.getName();

    OnShowMessageListener onShowMessageListener;
    OnChangeFragment onChangeFragment;

    @Inject AddImagePresenter presenter;
    GalleryComponent component;

    @BindView(R.id.text_input_layout_title) TextInputLayout textInputLayoutTitle;
    @BindView(R.id.text_input_link) TextInputLayout textInputLayoutLink;
    @BindView(R.id.text_input_layout_descr) TextInputLayout textInputLayoutDescr;

    @BindView(R.id.progress_bar) ProgressBar progressBar;
    @BindView(R.id.appbar) AppBarLayout appBar;
    @BindView(R.id.toolbar) Toolbar toolbar;

    @BindView(R.id.btn_addimage) FloatingActionButton ctaCreate;

    @OnClick(R.id.btn_addimage)
    public void clickCtaCreate() {
        this.presenter.createTask(
                textInputLayoutTitle.getEditText().getText().toString().trim(),
                textInputLayoutDescr.getEditText().getText().toString().trim(),
                textInputLayoutLink.getEditText().getText().toString().trim()
                );
    }

    @Override
    public void injectDependencies() {
        if (component == null) {
            component = DaggerGalleryComponent.builder()
                    .appComponent(App.get(getContext()).getComponent())
                    .build();
            component.inject(this);
        }
    }

    @Override
    public void attachToPresenter() {
        this.presenter.attachView(this);
    }

    @Override
    public void detachFromPresenter() {
        this.presenter.detachView();
    }

    public AddImageFragment() {
        // Required empty public constructor
    }

    public static AddImageFragment newInstance() {
        AddImageFragment fragment = new AddImageFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getArgs(savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        this.injectDependencies();
        this.attachToPresenter();
        super.onAttach(context);
        if (context instanceof OnShowMessageListener) {
            onShowMessageListener = (OnShowMessageListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnShowMessageListener");
        }
        if (context instanceof OnChangeFragment) {
            onChangeFragment = (OnChangeFragment) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnChangeFragment");
        }
    }

    @Override
    public void onDetach() {
        detachFromPresenter();
        super.onDetach();
    }

    @Override
    public void onLandscape() {
        // Empty intentionally.
    }

    @Override
    public void onPortrait() {
        // Empty intentionally.
    }

    @Override
    public void showLoading() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showMessage(String message) {
        if (onShowMessageListener != null) {
            onShowMessageListener.onShowMessage(message);
        }
    }

    @Override
    public void showNoNetwork() {
        if (onShowMessageListener != null) {
            onShowMessageListener.onShowMessage(getResources().getString(R.string.no_network));
        }
    }

    @Override
    public void onSuccess() {
        onChangeFragment.onChangeFragment(ShowcaseFragment.TAG, null);
    }

    @Override
    public void onFailure() {
        showMessage(getResources().getString(R.string.error_create_task));
    }

    @Override
    public void getArgs(Bundle _bundle) {
        // Empty intentionally.
    }
}
