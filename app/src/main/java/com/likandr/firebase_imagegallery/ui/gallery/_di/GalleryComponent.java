package com.likandr.firebase_imagegallery.ui.gallery._di;

import com.likandr.firebase_imagegallery.common._di.AppComponent;
import com.likandr.firebase_imagegallery.data.gallery.repositories.GalleryDataSourceRemote;
import com.likandr.firebase_imagegallery.data.gallery.repositories.GalleryRepository;
import com.likandr.firebase_imagegallery.data.image.repositories.ImageDataRepository;
import com.likandr.firebase_imagegallery.data.image.repositories.ImageDataSourceRemote;
import com.likandr.firebase_imagegallery.domain.image.interactors.AddImageUseCase;
import com.likandr.firebase_imagegallery.domain.gallery.interactors.GetGalleryUseCase;
import com.likandr.firebase_imagegallery.ui.ActivityScope;
import com.likandr.firebase_imagegallery.ui.gallery.add_image.AddImageFragment;
import com.likandr.firebase_imagegallery.ui.gallery.add_image.AddImagePresenter;
import com.likandr.firebase_imagegallery.ui.gallery.details.DetailsFragment;
import com.likandr.firebase_imagegallery.ui.gallery.details.DetailsPresenter;
import com.likandr.firebase_imagegallery.ui.gallery.showcase.ShowcaseFragment;
import com.likandr.firebase_imagegallery.ui.gallery.showcase.ShowcasePresenter;
import com.likandr.firebase_imagegallery.ui.onboard._di.UserModule;

import dagger.Component;

@ActivityScope
@Component(
        dependencies = AppComponent.class,
        modules = {GalleryModule.class, UserModule.class}
)

public interface GalleryComponent {

    //Fragments
    void inject(ShowcaseFragment view);
    void inject(DetailsFragment view);
    void inject(AddImageFragment view);

    // Presenters
    void inject(ShowcasePresenter presenter);
    void inject(DetailsPresenter presenter);
    void inject(AddImagePresenter presenter);

    // UseCases/Interactors
    void inject(GetGalleryUseCase useCase);
    void inject(AddImageUseCase useCase);

    // Repositories
    void inject(GalleryRepository repository);
    void inject(ImageDataRepository repository);

    // DataSources
    void inject(GalleryDataSourceRemote dataSource);
    void inject(ImageDataSourceRemote dataSource);
}
