package com.likandr.firebase_imagegallery.ui.onboard.register;

import android.content.Context;

import com.likandr.firebase_imagegallery.domain.user.UserModel;
import com.likandr.firebase_imagegallery.common.base.BaseMvpView;

public interface RegisterMVP {

    interface View extends BaseMvpView {
        void setErrorEmailField();
        void setErrorPasswordField();
        void onSuccess();
        void onFailure();
    }

    interface Presenter {
        void register(String email, String password);
        void writeUser(String uid, String username);

        void onRegisterSuccess(UserModel userModel);
        void onRegisterFailure(Throwable e);
        void onRegisterSuccessTracking(UserModel userModel);
        void onRegisterFailureTracking(Throwable e);

        void onWriteUserSuccess();
        void onWriteUserFailure(Throwable e);

        void goToBucket(Context context);
    }
}