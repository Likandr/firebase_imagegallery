package com.likandr.firebase_imagegallery.ui.gallery.details;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ProgressBar;

import com.likandr.firebase_imagegallery.R;
import com.likandr.firebase_imagegallery.common.App;
import com.likandr.firebase_imagegallery.common.base.BaseFragment;
import com.likandr.firebase_imagegallery.common.base.Layout;
import com.likandr.firebase_imagegallery.domain.image.ImageModel;
import com.likandr.firebase_imagegallery.ui.gallery._di.DaggerGalleryComponent;
import com.likandr.firebase_imagegallery.ui.gallery._di.GalleryComponent;

import javax.inject.Inject;

import butterknife.BindView;

@Layout(id = R.layout.fragment_details)
public class DetailsFragment extends BaseFragment implements DetailsMVP.View {

    public static final String TAG = DetailsFragment.class.getName();

    OnShowMessageListener onShowMessageListener;
    OnChangeFragment onChangeFragment;

    ImageModel mImageModel;

    @Inject DetailsPresenter presenter;
    GalleryComponent component;

    @BindView(R.id.progress_bar) ProgressBar progressBar;

    @Override
    public void injectDependencies() {
        if (component == null) {
            component = DaggerGalleryComponent.builder()
                    .appComponent(App.get(getContext()).getComponent())
                    .build();
            component.inject(this);
        }
    }

    @Override
    public void attachToPresenter() {
        this.presenter.attachView(this);
    }

    @Override
    public void detachFromPresenter() {
        this.presenter.detachView();
    }

    public DetailsFragment() {
        // Required empty public constructor
    }

    public static DetailsFragment newInstance() {
        DetailsFragment fragment = new DetailsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getArgs(savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        this.injectDependencies();
        this.attachToPresenter();
        super.onAttach(context);
        if (context instanceof OnShowMessageListener) {
            onShowMessageListener = (OnShowMessageListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnShowMessageListener");
        }
        if (context instanceof OnChangeFragment) {
            onChangeFragment = (OnChangeFragment) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnChangeFragment");
        }
    }

    @Override
    public void onDetach() {
        detachFromPresenter();
        super.onDetach();
    }

    @Override
    public void showImageData(ImageModel model) {
        // Empty intentionally.
    }

    @Override
    public void showImageDataError() {
        // Empty intentionally.
    }

    @Override
    public void onLandscape() {
        // Empty intentionally.
    }

    @Override
    public void onPortrait() {
        // Empty intentionally.

    }

    @Override
    public void showLoading() {
        progressBar.setVisibility(View.VISIBLE);

    }

    @Override
    public void hideLoading() {
        progressBar.setVisibility(View.GONE);

    }

    @Override
    public void showMessage(String message) {
        if (onShowMessageListener != null) {
            onShowMessageListener.onShowMessage(message);
        }
    }

    @Override
    public void showNoNetwork() {
        if (onShowMessageListener != null) {
            onShowMessageListener.onShowMessage(getResources().getString(R.string.no_network));
        }
    }

    @Override
    public void getArgs(Bundle _bundle) {
        // Empty intentionally.
    }

    @Override
    public void deleteImageData() {
        presenter.deleteImageData(mImageModel.getId());
    }

    @Override
    public void showTaskDeletedError(String message) {
        showMessage(message);
    }
}