package com.likandr.firebase_imagegallery.ui.onboard;

import android.content.Context;

import com.likandr.firebase_imagegallery.common.misc.Params;
import com.likandr.firebase_imagegallery.domain.user.interactors.CheckUserUseCase;
import com.likandr.firebase_imagegallery.common.misc.external.AnalyticsInterface;
import com.likandr.firebase_imagegallery.common.misc.external.ConfigInterface;
import com.likandr.firebase_imagegallery.firebase.FirebucketConfig;
import com.likandr.firebase_imagegallery.common.base.presenter.BasePresenter;
import com.likandr.firebase_imagegallery.common.misc.rx.DefaultObserver;
import com.likandr.firebase_imagegallery.ui.gallery.GalleryActivity;

import javax.inject.Inject;

public final class OnBoardingPresenter extends BasePresenter<OnBoardingMVP.View>
        implements OnBoardingMVP.Presenter {
    private final static String TAG = OnBoardingPresenter.class.getName();

    private final CheckUserUseCase checkUserUseCase;
    private final AnalyticsInterface analyticsInterface;
    private final ConfigInterface configInterface;

    @Inject
    public OnBoardingPresenter(CheckUserUseCase checkUserUseCase,
                               AnalyticsInterface analyticsInterface,
                               ConfigInterface configInterface) {
        this.checkUserUseCase = checkUserUseCase;
        this.analyticsInterface = analyticsInterface;
        this.configInterface = configInterface;
    }

    @Override
    public void attachView(OnBoardingMVP.View view) {
        super.attachView(view);
        analyticsInterface.trackPageView(AnalyticsInterface.VIEW_ONBOARDING);
    }

    @Override
    public void detachView() {
        checkUserUseCase.dispose();
        super.detachView();
    }

    @Override
    public void goToBucket(Context context) {
        GalleryActivity.startMe(context);
    }

    @Override
    public void checkIfUserIsLogged() {
        checkUserUseCase.execute(new CheckUserObserver(), Params.EMPTY);
    }

    @Override
    public boolean isInMaintenance() {
        final String value = configInterface.getString(FirebucketConfig.MAINTENANCE.getKey());
        return (!value.equals(FirebucketConfig.MAINTENANCE.getDefaultValue()));
    }

    private final class CheckUserObserver extends DefaultObserver<Boolean> {

        @Override public void onComplete() {
            super.onComplete();
        }

        @Override public void onError(Throwable e) {
            super.onError(e);
        }

        @Override public void onNext(Boolean value) {
            super.onNext(value);
            if (value) {
                view.userLogged();
            }
        }
    }
}