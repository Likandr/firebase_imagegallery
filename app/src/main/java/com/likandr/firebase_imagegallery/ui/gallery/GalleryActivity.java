package com.likandr.firebase_imagegallery.ui.gallery;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.util.Pair;
import android.view.View;

import com.likandr.firebase_imagegallery.R;
import com.likandr.firebase_imagegallery.common.base.BaseActivity;
import com.likandr.firebase_imagegallery.common.base.BaseFragment;
import com.likandr.firebase_imagegallery.common.base.Layout;
import com.likandr.firebase_imagegallery.ui.gallery.add_image.AddImageFragment;
import com.likandr.firebase_imagegallery.ui.gallery.details.DetailsFragment;
import com.likandr.firebase_imagegallery.ui.gallery.showcase.ShowcaseFragment;

import butterknife.BindView;

@Layout(id = R.layout.activity_gallery)
public class GalleryActivity extends BaseActivity implements
        BaseFragment.OnShowMessageListener,
        BaseFragment.OnChangeFragment{

    @BindView(R.id.root_view) CoordinatorLayout rootView;

    private String currentFragmentTag;

    public static void startMe(Context context) {
        Intent intent = new Intent(context, GalleryActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        context.startActivity(intent);
    }

    public static Intent getIntentForNotification(Context context) {
        Intent intent = new Intent(context, GalleryActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.getExtras(getIntent());
        this.setUpToolbar();

        initFragment(ShowcaseFragment.TAG, null);
    }

    private void initFragment(String fragmentTag,
                              @Nullable Pair<View, String> sharedElement) {
        currentFragmentTag = fragmentTag;
        Fragment fragment = getFragment(fragmentTag);

        FragmentTransaction fragmentTransaction = getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.content_frame, fragment);

        if (sharedElement != null) {
            fragmentTransaction.addSharedElement(sharedElement.first, sharedElement.second);
            fragment.setSharedElementEnterTransition(getWindow().getSharedElementEnterTransition());
            fragment.setSharedElementReturnTransition(getWindow().getSharedElementReturnTransition());
        }
        fragmentTransaction.replace(R.id.content_frame, fragment).commit();
    }

    private Fragment getFragment(String fragmentTag) {

        if (fragmentTag.equals(AddImageFragment.TAG)) {
            return AddImageFragment.newInstance();
        }

        if (fragmentTag.equals(DetailsFragment.TAG)) {
            return DetailsFragment.newInstance();
        }

        return ShowcaseFragment.newInstance();
    }

    @Override
    public void getExtras(Intent _intent) {

    }

    @Override
    public void closeActivity() {
        this.finish();
    }

    @Override
    public void setUpToolbar() {

    }

    @Override
    public Fragment getAttachedFragment(int id) {
        return getSupportFragmentManager().findFragmentById(id);
    }

    @Override
    public Fragment getAttachedFragment(String tag) {
        return getSupportFragmentManager().findFragmentByTag(tag);
    }

    @Override
    public void onShowMessage(String message) {
        //SnackBarUtils.showSimpleSnackbar(rootView, message);
    }

    @Override
    public void onChangeFragment(String fragmentTag, @Nullable Pair<View, String> sharedElement) {
        initFragment(fragmentTag, sharedElement);
    }

    @Override
    public void onBackPressed() {
        if (!currentFragmentTag.equals(ShowcaseFragment.TAG)) {
            initFragment(ShowcaseFragment.TAG, null);
        } else {
            finish();
        }
    }
}