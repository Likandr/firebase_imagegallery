package com.likandr.firebase_imagegallery.ui.gallery.showcase;

import android.content.Context;

import com.likandr.firebase_imagegallery.common.base.BaseMvpView;
import com.likandr.firebase_imagegallery.domain.gallery.GalleryModel;
import com.likandr.firebase_imagegallery.domain.image.ImageModel;

import java.util.ArrayList;

public class ShowcaseMVP {

    interface View extends BaseMvpView {
        void showGallery(ArrayList<ImageModel> model);
        void showGalleryEmpty();
        void showGalleryEmptyFirstTime();
        void showGalleryError();
    }

    interface Presenter {
        void loadData();

        void onLoadDataSuccess(GalleryModel galleryModel);
        void onLoadDataFailure(Throwable e);

        void onLoadDataSuccessTracking();
        void onLoadDataFailureTracking(Throwable e);

        void goToDetails(Context context);
    }
}