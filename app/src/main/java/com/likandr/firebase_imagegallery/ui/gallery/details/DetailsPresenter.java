package com.likandr.firebase_imagegallery.ui.gallery.details;

import android.content.SharedPreferences;
import android.os.Bundle;

import com.likandr.firebase_imagegallery.common.base.presenter.BasePresenter;
import com.likandr.firebase_imagegallery.common.misc.Params;
import com.likandr.firebase_imagegallery.common.misc.external.AnalyticsInterface;
import com.likandr.firebase_imagegallery.common.misc.helpers.SharedPreferencesHelper;
import com.likandr.firebase_imagegallery.common.misc.rx.DefaultObserver;
import com.likandr.firebase_imagegallery.domain.image.ImageModel;
import com.likandr.firebase_imagegallery.domain.image.interactors.DeleteImageUseCase;
import com.likandr.firebase_imagegallery.domain.image.interactors.GetImageUseCase;

import javax.inject.Inject;

public final class DetailsPresenter extends BasePresenter<DetailsMVP.View> implements DetailsMVP.Presenter {

    private final static String TAG = DetailsPresenter.class.getName();

    private final AnalyticsInterface analyticsInterface;
    private final SharedPreferences sharedPreferences;
    private final GetImageUseCase getImageUseCase;
    private final DeleteImageUseCase deleteImageUseCase;

    @Inject
    public DetailsPresenter(GetImageUseCase getImageUseCase,
                            DeleteImageUseCase deleteImageUseCase,
                            AnalyticsInterface analyticsInterface,
                            SharedPreferences sharedPreferences) {
        this.getImageUseCase = getImageUseCase;
        this.deleteImageUseCase = deleteImageUseCase;
        this.analyticsInterface = analyticsInterface;
        this.sharedPreferences = sharedPreferences;
    }

    @Override
    public void attachView(DetailsMVP.View view) {
        super.attachView(view);
        analyticsInterface.trackPageView(AnalyticsInterface.VIEW_DETAILS);
    }

    @Override
    public void detachView() {
        getImageUseCase.dispose();
        deleteImageUseCase.dispose();
        super.detachView();
    }

    private String getIdKey() {
        return SharedPreferencesHelper.getString(sharedPreferences, SharedPreferencesHelper.KEY_DETAIL_ID_IMAGEDATA);
    }

    @Override
    public void getImageData() {
        view.showLoading();

        Params params = Params.create();
        params.putString(GetImageUseCase.PARAMS_KEY_ID_IMAGE, getIdKey());
        getImageUseCase.execute(new GetImageDataObserver(), params);
    }

    @Override
    public void onImageDataSuccess(ImageModel imageModel) {
        checkViewAttached();
        view.hideLoading();
        if (imageModel != null) {
            view.showImageData(imageModel);
        }
    }

    @Override
    public void onImageDataFailure(Throwable e) {
        e.printStackTrace();
        checkViewAttached();
        view.hideLoading();
        view.showImageDataError();
    }

    @Override
    public void onImageDataSuccessTracking() {
        analyticsInterface.trackGetImageDataSuccess(null);
    }

    @Override
    public void onImageDataFailureTracking(Throwable e) {
        Bundle bundle = new Bundle();
        bundle.putString(AnalyticsInterface.PARAM_MESSAGE, e.getMessage());
        analyticsInterface.trackGetImageDataFailure(bundle);
    }

    @Override
    public void deleteImageData(String taskId) {
        Params params = Params.create();
        params.putString(DeleteImageUseCase.PARAMS_KEY_IMAGE_ID, taskId);
        deleteImageUseCase.execute(new DeleteImageDataObserver(), params);
    }

    @Override
    public void onDeleteImageDataSuccess() {
        checkViewAttached();
    }

    @Override
    public void onDeleteImageDataFailure(Throwable e) {
        checkViewAttached();
        view.showTaskDeletedError(e.getMessage());
    }

    @Override
    public void onDeleteImageDataSuccessTracking() {
        analyticsInterface.trackDeleteImageSuccess(null);
    }

    @Override
    public void onDeleteImageDataFailureTracking(Throwable e) {
        Bundle bundle = new Bundle();
        bundle.putString(AnalyticsInterface.PARAM_MESSAGE, e.getMessage());
        analyticsInterface.trackDeleteImageFailure(bundle);
    }

    private final class GetImageDataObserver extends DefaultObserver<ImageModel> {

        @Override public void onComplete() {
            super.onComplete();
        }

        @Override public void onError(Throwable e) {
            super.onError(e);
            onImageDataFailure(e);
            onImageDataFailureTracking(e);
        }

        @Override public void onNext(ImageModel imageModel) {
            super.onNext(imageModel);
            onImageDataSuccess(imageModel);
            onImageDataSuccessTracking();
        }
    }

    private final class DeleteImageDataObserver extends DefaultObserver<ImageModel> {

        @Override public void onComplete() {
            super.onComplete();
        }

        @Override public void onError(Throwable e) {
            super.onError(e);
            onDeleteImageDataFailureTracking(e);
            onDeleteImageDataFailure(e);
        }

        @Override public void onNext(ImageModel imageModel) {
            super.onNext(imageModel);
            onDeleteImageDataSuccessTracking();
            onDeleteImageDataSuccess();
        }
    }
}