package com.likandr.firebase_imagegallery.ui.gallery.add_image;

import com.likandr.firebase_imagegallery.common.base.BaseMvpView;
import com.likandr.firebase_imagegallery.domain.image.ImageModel;

public class AddImageMVP {

    interface View extends BaseMvpView {
        void onSuccess();
        void onFailure();
    }

    interface Presenter {
        void createTask(String title,
                        String descr,
                        String url_original);

        void onCreateTaskSuccess(ImageModel imageModel);
        void onCreateTaskFailure(Throwable e);

        void onCreateTaskSuccessTracking(ImageModel imageModel);
        void onCreateTaskFailureTracking(Throwable e);
    }
}
