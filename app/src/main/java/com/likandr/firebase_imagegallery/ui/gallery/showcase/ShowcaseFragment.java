package com.likandr.firebase_imagegallery.ui.gallery.showcase;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewStub;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.cremy.greenrobotutils.library.ui.SnackBarUtils;
import com.likandr.firebase_imagegallery.R;
import com.likandr.firebase_imagegallery.common.App;
import com.likandr.firebase_imagegallery.common.base.BaseFragment;
import com.likandr.firebase_imagegallery.common.base.Layout;
import com.likandr.firebase_imagegallery.common.misc.ui.GridInsetDecoration;
import com.likandr.firebase_imagegallery.common.misc.widgets.MaterialDesignFlatButton;
import com.likandr.firebase_imagegallery.domain.image.ImageModel;
import com.likandr.firebase_imagegallery.ui.gallery._di.DaggerGalleryComponent;
import com.likandr.firebase_imagegallery.ui.gallery._di.GalleryComponent;
import com.likandr.firebase_imagegallery.ui.gallery.add_image.AddImageFragment;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;

@Layout(id = R.layout.fragment_showcase)
public class ShowcaseFragment extends BaseFragment implements ShowcaseMVP.View, View.OnClickListener {

    public static final String TAG = ShowcaseFragment.class.getName();
    private static final int COLUMNS_COUNT = 2;

    OnShowMessageListener onShowMessageListener;
    OnChangeFragment onChangeFragment;

    Toolbar mToolbar;

    @BindView(R.id.root_view)                       CoordinatorLayout rootView;
    @BindView(R.id.recycler_view)                   RecyclerView recyclerView;
    @BindView(R.id.progress_bar)                    ProgressBar progressBar;
    @Nullable @BindView(R.id.cta_create)            FloatingActionButton ctaCreate;
    @Nullable @BindView(R.id.container_placeholder) FrameLayout containerPlaceholder;

    @OnClick(R.id.cta_create)
    public void clickCtaCreateTask() {
        onChangeFragment.onChangeFragment(AddImageFragment.TAG, null);
    }

    @Inject Picasso mPicasso;
    @Inject ShowcasePresenter presenter;
    GalleryComponent component;

    ShowcaseAdapter adapter;

    @Override
    public void injectDependencies() {
        if (component == null) {
            component = DaggerGalleryComponent.builder()
                    .appComponent(App.get(getContext()).getComponent())
                    .build();
            component.inject(this);
        }
    }

    @Override
    public void attachToPresenter() {
        this.presenter.attachView(this);
    }

    @Override
    public void detachFromPresenter() {
        this.presenter.detachView();
    }

    public ShowcaseFragment() {
        // Required empty public constructor
    }

    public static ShowcaseFragment newInstance() {
        ShowcaseFragment fragment = new ShowcaseFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getArgs(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        presenter.loadData();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_gallery, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onAttach(Context context) {
        this.injectDependencies();
        this.attachToPresenter();
        super.onAttach(context);
        if (context instanceof OnShowMessageListener) {
            onShowMessageListener = (OnShowMessageListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnShowMessageListener");
        }
        if (context instanceof OnChangeFragment) {
            onChangeFragment = (OnChangeFragment) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnChangeFragment");
        }
    }

    @Override
    public void onDetach() {
        detachFromPresenter();
        super.onDetach();
    }

    @Override
    public void showGallery(ArrayList<ImageModel> model) {
        recyclerView.setVisibility(View.VISIBLE);
        if (containerPlaceholder != null) {
            containerPlaceholder.setVisibility(View.GONE);
        }

        if (adapter != null) {
            adapter.setItems(model);
        } else {
            adapter = new ShowcaseAdapter(getActivity(), model, mPicasso);
            adapter.setOnClickListener((image, view) -> {
                //onChangeFragment.onChangeFragment(DetailsFragment.TAG, null);
            });

            final StaggeredGridLayoutManager layout = new StaggeredGridLayoutManager(COLUMNS_COUNT, StaggeredGridLayoutManager.VERTICAL);
            recyclerView.setLayoutManager(layout);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.addItemDecoration(new GridInsetDecoration(getContext(), R.dimen.grid_inset));
            recyclerView.setAdapter(adapter);
        }
    }

    @Override
    public void showGalleryEmpty() {
        prepareViewStub();

        final TextView textviewPlaceholder = (TextView) containerPlaceholder.findViewById(R.id.textview_placeholder);
        final MaterialDesignFlatButton ctaPlaceholder = (MaterialDesignFlatButton) containerPlaceholder.findViewById(R.id.cta_placeholder);
        final ImageView imageviewPlaceholder = (ImageView) containerPlaceholder.findViewById(R.id.imageview_placeholder);

        textviewPlaceholder.setText(getString(R.string.bucket_placeholder_empty_title));
        ctaPlaceholder.setText(getString(R.string.bucket_activity_placeholder_button_text));
        ctaPlaceholder.setOnClickListener(this);
        ctaPlaceholder.setVisibility(View.GONE);
        imageviewPlaceholder.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.image_bucket_empty));
    }

    @Override
    public void showGalleryEmptyFirstTime() {
        prepareViewStub();

        final TextView textviewPlaceholder = (TextView) containerPlaceholder.findViewById(R.id.textview_placeholder);
        final MaterialDesignFlatButton ctaPlaceholder = (MaterialDesignFlatButton) containerPlaceholder.findViewById(R.id.cta_placeholder);
        final ImageView imageviewPlaceholder = (ImageView) containerPlaceholder.findViewById(R.id.imageview_placeholder);

        textviewPlaceholder.setText(getString(R.string.bucket_placeholder_empty_first_time_title));
        ctaPlaceholder.setText(getString(R.string.bucket_activity_placeholder_button_text));
        ctaPlaceholder.setVisibility(View.GONE);
        imageviewPlaceholder.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.image_logo_bucket));
    }

    @Override
    public void showGalleryError() {
        prepareViewStub();

        final TextView textviewPlaceholder = (TextView) containerPlaceholder.findViewById(R.id.textview_placeholder);
        final MaterialDesignFlatButton ctaPlaceholder = (MaterialDesignFlatButton) containerPlaceholder.findViewById(R.id.cta_placeholder);
        final ImageView imageviewPlaceholder = (ImageView) containerPlaceholder.findViewById(R.id.imageview_placeholder);

        textviewPlaceholder.setText(getString(R.string.bucket_placeholder_error_title));
        ctaPlaceholder.setText(getString(R.string.bucket_activity_placeholder_button_text));
        ctaPlaceholder.setOnClickListener(this);
        ctaPlaceholder.setVisibility(View.VISIBLE);
        imageviewPlaceholder.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.image_bucket_error));
    }

    private void prepareViewStub() {
        recyclerView.setVisibility(View.GONE);

        if (containerPlaceholder == null) {
            containerPlaceholder = (FrameLayout) ((ViewStub) getView().findViewById(R.id.viewstub_bucket)).inflate();
        }
        containerPlaceholder.setVisibility(View.VISIBLE);
    }

    @Override
    public void showLoading() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showMessage(String message) {
        if (onShowMessageListener != null) {
            onShowMessageListener.onShowMessage(message);
        }
    }

    @Override
    public void onLandscape() {
        // Empty intentionally.
    }

    @Override
    public void onPortrait() {
        // Empty intentionally.
    }

    @Override
    public void showNoNetwork() {
        SnackBarUtils.showSimpleSnackbar(this.rootView,
                getResources().getString(R.string.no_network));
    }

    @Override
    public void getArgs(Bundle _bundle) {
        // Empty intentionally.
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.cta_placeholder) {
            presenter.loadData();
        }
    }
}