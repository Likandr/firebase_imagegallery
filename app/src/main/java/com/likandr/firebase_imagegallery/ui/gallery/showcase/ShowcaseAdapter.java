package com.likandr.firebase_imagegallery.ui.gallery.showcase;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.likandr.firebase_imagegallery.R;
import com.likandr.firebase_imagegallery.domain.image.ImageModel;
import com.squareup.picasso.Picasso;

import java.util.Collections;
import java.util.List;

public class ShowcaseAdapter extends RecyclerView.Adapter<ShowcaseAdapter.ViewHolder> {
    private List<ImageModel> mGallery = Collections.emptyList();

    private final Picasso picasso;
    private final LayoutInflater inflater;

    @Nullable
    private OnClickListener mOnClickListener;

    public ShowcaseAdapter(Context context, List<ImageModel> images, Picasso picasso) {
        this.picasso = picasso;
        this.mGallery = images;
        inflater = LayoutInflater.from(context);
    }

    public void setOnClickListener(@Nullable OnClickListener onClickListener) {
        this.mOnClickListener = onClickListener;
    }

    public void setItems(List<ImageModel> gallery) {
        this.mGallery = gallery;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ShowcaseItem view = (ShowcaseItem) inflater.inflate(R.layout.showcase_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.bindTo(mGallery.get(position), picasso);
        holder.itemView.setOnClickListener(v -> {
            if (mOnClickListener != null) {
                mOnClickListener.onImageClicked(mGallery.get(position), (ShowcaseItem) holder.itemView);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mGallery.size();
    }

    public static final class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(ShowcaseItem itemView) {
            super(itemView);
        }

        public void bindTo(ImageModel image, Picasso picasso) {
            ((ShowcaseItem) itemView).bindTo(image, picasso);
        }
    }

    public interface OnClickListener {
        void onImageClicked(ImageModel image, ShowcaseItem view);
    }
}