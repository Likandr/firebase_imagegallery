package com.likandr.firebase_imagegallery.ui.gallery.showcase;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.view.ViewTreeObserver;
import android.widget.TextView;

import com.likandr.firebase_imagegallery.R;
import com.likandr.firebase_imagegallery.common.misc.ui.AspectRatioImageView;
import com.likandr.firebase_imagegallery.domain.image.ImageModel;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ShowcaseItem  extends CardView {
    @BindView(R.id.gallery_image_image) AspectRatioImageView image;
    @BindView(R.id.gallery_image_title) TextView title;

    public ShowcaseItem(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);
    }

    public void bindTo(final ImageModel item, final Picasso picasso) {
        float heightRatio = 1;//item.height / (float) item.width;
        image.setHeightRatio(heightRatio);
        image.requestLayout();
        image.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                image.getViewTreeObserver().removeOnPreDrawListener(this);
                final int height = image.getMeasuredHeight();
                final int width = image.getMeasuredWidth();
                picasso.load(item.getUrl_original()).resize(width, height).into(image);
                return true;
            }
        });

        title.setText(item.getTitle());
    }
}