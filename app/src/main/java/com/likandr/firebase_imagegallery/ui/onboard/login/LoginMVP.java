package com.likandr.firebase_imagegallery.ui.onboard.login;

import android.content.Context;

import com.likandr.firebase_imagegallery.domain.user.UserModel;
import com.likandr.firebase_imagegallery.common.base.BaseMvpView;

public interface LoginMVP {

    interface View extends BaseMvpView {
        void setErrorEmailField();
        void setErrorPasswordField();
        void onSuccess();
        void onFailure();
    }

    interface Presenter {
        void login(String email, String password);
        void onLoginSuccess(UserModel userModel);
        void onLoginFailure(Throwable e);

        void onLoginSuccessTracking(UserModel userModel);
        void onLoginFailureTracking(Throwable e);

        void goToGallery(Context context);
    }
}