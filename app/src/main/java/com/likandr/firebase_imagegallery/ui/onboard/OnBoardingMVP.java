package com.likandr.firebase_imagegallery.ui.onboard;

import android.content.Context;

import com.likandr.firebase_imagegallery.common.base.BaseMvpView;

public interface OnBoardingMVP {

    interface View extends BaseMvpView {
        void userLogged();
    }

    interface Presenter {
        void goToBucket(Context context);
        void checkIfUserIsLogged();

        boolean isInMaintenance();
    }
}