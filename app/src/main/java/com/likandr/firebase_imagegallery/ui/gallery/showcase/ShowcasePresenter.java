package com.likandr.firebase_imagegallery.ui.gallery.showcase;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import com.likandr.firebase_imagegallery.common.base.presenter.BasePresenter;
import com.likandr.firebase_imagegallery.common.misc.Params;
import com.likandr.firebase_imagegallery.common.misc.external.AnalyticsInterface;
import com.likandr.firebase_imagegallery.common.misc.helpers.SharedPreferencesHelper;
import com.likandr.firebase_imagegallery.common.misc.rx.DefaultObserver;
import com.likandr.firebase_imagegallery.domain.gallery.GalleryModel;
import com.likandr.firebase_imagegallery.domain.gallery.interactors.GetGalleryUseCase;

import javax.inject.Inject;

public final class ShowcasePresenter extends BasePresenter<ShowcaseMVP.View> implements ShowcaseMVP.Presenter {

    private final static String TAG = ShowcasePresenter.class.getName();

    private final AnalyticsInterface analyticsInterface;
    private final SharedPreferences sharedPreferences;
    private final GetGalleryUseCase getGalleryUseCase;

    @Inject
    public ShowcasePresenter(GetGalleryUseCase getGalleryUseCase,
                           AnalyticsInterface analyticsInterface,
                           SharedPreferences sharedPreferences) {
        this.getGalleryUseCase = getGalleryUseCase;
        this.analyticsInterface = analyticsInterface;
        this.sharedPreferences = sharedPreferences;
    }

    @Override
    public void attachView(ShowcaseMVP.View view) {
        super.attachView(view);
        analyticsInterface.trackPageView(AnalyticsInterface.VIEW_SHOWCASE);
    }

    @Override
    public void detachView() {
        getGalleryUseCase.dispose();
        super.detachView();
    }

    @Override
    public void loadData() {
        view.showLoading();
        getGalleryUseCase.execute(new GetImagesObserver(), Params.EMPTY);
    }

    @Override
    public void onLoadDataSuccess(GalleryModel galleryModel) {
        checkViewAttached();
        view.hideLoading();
        if (galleryModel.isEmpty()) {
            if (!SharedPreferencesHelper.getBoolean(sharedPreferences,
                    SharedPreferencesHelper.KEY_USER_TASK_AT_LEAST_ONCE)) {
                view.showGalleryEmptyFirstTime();
            } else {
                view.showGalleryEmpty();
            }
        } else {
            SharedPreferencesHelper.putBoolean(sharedPreferences,
                    SharedPreferencesHelper.KEY_USER_TASK_AT_LEAST_ONCE,
                    true);
            view.showGallery(galleryModel.toDisplayedList());
        }
    }

    @Override
    public void onLoadDataFailure(Throwable e) {
        e.printStackTrace();
        checkViewAttached();
        view.hideLoading();
        view.showGalleryError();
    }

    @Override
    public void onLoadDataSuccessTracking() {
        analyticsInterface.trackGetGallerySuccess(null);
    }

    @Override
    public void onLoadDataFailureTracking(Throwable e) {
        Bundle bundle = new Bundle();
        bundle.putString(AnalyticsInterface.PARAM_MESSAGE, e.getMessage());
        analyticsInterface.trackGetGalleryFailure(bundle);
    }

    @Override
    public void goToDetails(Context context) {

    }

    private final class GetImagesObserver extends DefaultObserver<GalleryModel> {

        @Override public void onComplete() {
            super.onComplete();
        }

        @Override public void onError(Throwable e) {
            super.onError(e);
            onLoadDataFailureTracking(e);
            onLoadDataFailure(e);
        }

        @Override public void onNext(GalleryModel galleryModel) {
            super.onNext(galleryModel);
            onLoadDataSuccessTracking();
            onLoadDataSuccess(galleryModel);
        }
    }
}