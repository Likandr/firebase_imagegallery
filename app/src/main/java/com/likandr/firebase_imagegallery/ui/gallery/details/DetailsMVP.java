package com.likandr.firebase_imagegallery.ui.gallery.details;

import com.likandr.firebase_imagegallery.common.base.BaseMvpView;
import com.likandr.firebase_imagegallery.domain.image.ImageModel;

public class DetailsMVP {

    interface View extends BaseMvpView {
        void showImageData(ImageModel model);
        void showImageDataError();

        void deleteImageData();
        void showTaskDeletedError(String message);
    }

    interface Presenter {
        void getImageData();
        void onImageDataSuccess(ImageModel imageModel);
        void onImageDataFailure(Throwable e);

        void onImageDataSuccessTracking();
        void onImageDataFailureTracking(Throwable e);

        void deleteImageData(String taskId);

        void onDeleteImageDataSuccess();
        void onDeleteImageDataFailure(Throwable e);

        void onDeleteImageDataSuccessTracking();
        void onDeleteImageDataFailureTracking(Throwable e);
    }
}