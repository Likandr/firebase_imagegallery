# README #

## !! The application is in development !! ##

### Used  ###

* MVP
* Firebase(auth, db, analitics)
* Rxjava2
* RxAndroid
* Dagger2
* ButterKnife
* LeakCanary
* Lambda, java8

### The plans ###
* Translation of the code into Kotlin
* Сover tests
* More frendly ui

### For communication ###
* [Telegram](https://t.me/Likandr)
* [e-mail](mailto:sidorenko.nikolay.a@gmail.com)